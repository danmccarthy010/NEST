package edu.ncc.nest.nestapp;
/**
 *

 Copyright (C) 2022 The LibreFoodPantry Developers.


 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.


 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.


 You should have received a copy of the GNU General Public License
 along with this program.  If not, see https://www.gnu.org/licenses/.
 */
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class TextNotification extends AppCompatActivity{
    private Button mButton;
    private EditText editTextNumber;
    private EditText editTextMessage;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_message);

        ActivityCompat.requestPermissions(TextNotification.this, new String[]{Manifest.permission.SEND_SMS,
                Manifest.permission.READ_SMS}, PackageManager.PERMISSION_GRANTED);
        editTextMessage = findViewById(R.id.msgTxt);
        editTextNumber = findViewById(R.id.editTextNumber);
        mButton = (Button) findViewById(R.id.sendBtn);
        mButton.setOnClickListener(myListener);
    }
    public void sendSMS(View view) {
        String message = editTextMessage.getText().toString();
        String number = editTextNumber.getText().toString();
        SmsManager mySMSManager = SmsManager.getDefault();
        mySMSManager.sendTextMessage(number, null, message, null, null);
    }

    private View.OnClickListener myListener = new View.OnClickListener() {
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.sendBtn:
                    sendSMS(findViewById(R.id.sendBtn));
                    Toast toast = Toast.makeText(getApplicationContext(), "Text Sent.", Toast.LENGTH_LONG);
                    toast.show();
            }
        }
    };
}
